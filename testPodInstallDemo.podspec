
Pod::Spec.new do |spec|
 
  spec.name            = "testPodInstallDemo"
  spec.version         = "0.0.3"
  spec.summary         = "A short description of testPodInstallDemo."
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage        = "https://github.com/focuswei/testPodInstallDemo"
  spec.author          = { "focuswei" => "w394966935@gmail.com" }
  spec.platform        = :ios, "12.0"
  spec.source          = { :git => "https://gitlab.com/focuswei/myliviroboA8", :tag => "#{spec.version}" }
  spec.swift_version   = "5.0"
  spec.framework     = "UIKit","Foundation"
  #spec.source_files  = "testPodInstallDemo/*.swift"
  spec.vendored_frameworks = "testPodInstallDemo.framework"
  spec.pod_target_xcconfig = { "EXCLUDED_ARCHS[sdk=iphonesimulator*]" => "arm64" }
  spec.user_target_xcconfig = { "EXCLUDED_ARCHS[sdk=iphonesimulator*]" => "arm64" }
end
